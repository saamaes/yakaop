extends RigidBody

var boom

func _ready():
	boom = get_node("AudioStreamPlayer")

func _on_RigidBody_body_entered(body):
	boom.play()
