extends CollisionShape

var crouchAnim

func _ready():
	crouchAnim = get_node("AnimationPlayer")
func _input(event):
	if Input.is_action_just_pressed("crouch"):
		crouchAnim.play("Crouch")
	if Input.is_action_just_released("crouch"):
		crouchAnim.play_backwards("Crouch")
