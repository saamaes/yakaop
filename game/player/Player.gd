extends KinematicBody

var sense_x = 1
var sense_y = 1

var walkAccel = 100
var walkSpeed = 4
var stopForce = 100
var airControlMultiplier = 0
var jumpForce = 4
var gravity = 9.8
# I'll figure this out eventually
#var drag = 0.023

var head
var velocity = Vector3(0, 0, 0)
var upDirection = Vector3(0, 1, 0)

func _ready():
	head = get_node("Head")

func _input(event):
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		
		# Rotate one degree for one pixel of mouse movement.
		self.rotate_y(-event.relative.x / 360 * sense_x)
		head.rotate_x(-event.relative.y / 360 * sense_y)
		
		# Stop the player from rolling their heads too far forward/back
		head.rotation_degrees.x = clamp(head.rotation_degrees.x, -90, 90)

func _physics_process(delta):
	# Adjust for the physics framerate
	var gravityAccel = gravity * delta
	
	var movementForce = Vector3(0, 0, 0)
	
	if Input.is_action_pressed("forwards"):
		movementForce.z = -1
	if Input.is_action_pressed("backwards"):
		movementForce.z =  1
	if Input.is_action_pressed("left"):
		movementForce.x = -1
	if Input.is_action_pressed("right"):
		movementForce.x =  1
	
	movementForce = movementForce.normalized()
	movementForce *= walkAccel
	
	if self.is_on_floor():
		if movementForce:
			velocity += self.get_transform().basis.xform(movementForce * delta)
			if velocity.length() > walkSpeed:
				velocity -= velocity.normalized() * delta * stopForce
		else:
			if velocity.length() < delta * stopForce:
				velocity *= 0
			velocity -= velocity.normalized() * delta * stopForce
		
		if Input.is_action_pressed("jump"):
			velocity.y += jumpForce
	else:
		if movementForce:
			velocity += self.get_transform().basis.xform(movementForce * delta * airControlMultiplier)
			#if velocity.length() > walkSpeed:
				#var velocity2D = Vector2(velocity.x, velocity.z)
				#velocity2D -= velocity2D.normalized() * delta * stopForce
				#velocity = Vector3(velocity2D.x, velocity.y, velocity2D.y)
		#else:
			#var velocity2D = Vector2(velocity.x, velocity.z)
			#velocity2D -= velocity2D.normalized() * delta * stopForce
			#velocity = Vector3(velocity2D.x, velocity.y, velocity2D.y)
	
	velocity.y -= gravityAccel
	
	# Move and grab current velocity in case of collision
	velocity = self.move_and_slide(velocity, upDirection)
