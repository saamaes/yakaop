extends Control

func _on_Button_pressed():
	togglePause()

func _input(event):
	if event.is_action_pressed("pause"):
		togglePause()

func togglePause():
	if self.visible:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		self.visible = false
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		self.visible = true
